defmodule VoldersContractManager.AccountsTest do
  use VoldersContractManager.DataCase

  alias VoldersContractManager.Accounts

  describe "users" do
    alias VoldersContractManager.Accounts.User

    @valid_attrs %{email: "test@mailinator.com", name: "Test name", password: "testpassword", password_confirmation: "testpassword"}
    @invalid_attrs %{email: nil, name: nil, password_hash: nil, password_salt: nil}

    def assert_equal(%User{} = a, %User{} = b) do
      assert a.email == b.email
      assert a.name == b.name
    end

    def assert_equal(%{} = a, %User{} = b) do
      assert_equal(struct(User, a), b)
    end

    def assert_equal(%User{} = a, %{} = b) do
      assert_equal(a, struct(User, b))
    end

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Accounts.create_user()

      user
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      db_user = Accounts.get_user!(user.id)
      
      assert_equal(user, db_user)
    end
 
    test "get_user_by returns the user given email" do
      user = user_fixture()
      db_user = Accounts.get_user_by(email: user.email)

      assert_equal(user, db_user)
    end

    test "create_user/1 with valid data creates a user" do
      data = @valid_attrs

      assert {:ok, %User{} = user} = Accounts.create_user(@valid_attrs)
      
      assert_equal(data, user)
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_user(@invalid_attrs)
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = Accounts.change_user(user)
    end
  end
end
