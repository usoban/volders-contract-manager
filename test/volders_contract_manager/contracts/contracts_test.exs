defmodule VoldersContractManager.ContractsTest do
  use VoldersContractManager.DataCase

  alias VoldersContractManager.Accounts
  alias VoldersContractManager.Vendors
  alias VoldersContractManager.Categories
  alias VoldersContractManager.Contracts

  describe "contracts" do
    alias VoldersContractManager.Contracts.Contract

    @vendor %{name: "Vodafone"}
    @category %{name: "Mobile"}
    
    @user1 %{email: "test@mailinator.com", name: "Test name", password: "testpassword", password_confirmation: "testpassword"}
    @user2 %{email: "test2@mailinator.com", name: "Test name 2", password: "testpassword", password_confirmation: "testpassword"}

    @valid_attrs %{costs: "120.5", ends_at: Date.add(Date.utc_today, 2)}
    @update_attrs %{costs: "456.7", ends_at: Date.add(Date.utc_today, 5)}
    @invalid_attrs %{costs: nil, ends_at: nil}

    def fixture(:contract, %Accounts.User{id: uid}) do
      %{id: vid} = fixture(:vendor)
      %{id: cid} = fixture(:category)

      params = 
        @valid_attrs
        |> Map.put(:vendor_id, vid)
        |> Map.put(:category_id, cid)
        |> Map.put(:user_id, uid)

      {:ok, contract} = Contracts.create_contract(params)
      contract
    end

    def fixture(:vendor) do
      {:ok, vendor} = Vendors.create_vendor(@vendor)
      vendor
    end

    def fixture(:category) do
      {:ok, category} = Categories.create_category(@category)
      category
    end

    def fixture(:user, 1) do
      {:ok, user} = Accounts.create_user(@user1)
      user
    end

    def fixture(:user, 2) do
      {:ok, user} = Accounts.create_user(@user2)
      user
    end

    def contract_fixture(attrs \\ %{}) do
      {:ok, contract} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Contracts.create_contract()

      contract
    end

    test "list_contracts/0 returns all contracts" do
      %{id: cid} = contract = fixture(:contract, fixture(:user, 1))

      contracts = Contracts.list_contracts()

      assert 1 = length(contracts)
      assert  [%Contract{id: ^cid}] = contracts
    end

    test "get_contract!/1 returns the contract with given id" do
      %{id: cid} = contract = fixture(:contract, fixture(:user, 1))

      assert %{id: ^cid} = Contracts.get_contract!(contract.id)
    end

    test "create_contract/1 with valid data creates a contract" do
      %{id: vid} = fixture(:vendor)
      %{id: cid} = fixture(:category)
      %{id: uid} = fixture(:user, 1)

      params = 
        @valid_attrs
        |> Map.put(:vendor_id, vid)
        |> Map.put(:category_id, cid)
        |> Map.put(:user_id, uid)

      assert {:ok, %Contract{} = contract} = Contracts.create_contract(params)
      assert contract.costs == Decimal.new("120.5")
      assert contract.ends_at == Date.add(Date.utc_today, 2)
    end

    test "create_contract/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Contracts.create_contract(@invalid_attrs)
    end

    test "update_contract/2 with valid data updates the contract" do
      contract = fixture(:contract, fixture(:user, 1))
      assert {:ok, contract} = Contracts.update_contract(contract, @update_attrs)
      assert %Contract{} = contract
      assert contract.costs == Decimal.new("456.7")
      assert contract.ends_at == Date.add(Date.utc_today, 5)
    end

    test "update_contract/2 with invalid data returns error changeset" do
      contract = fixture(:contract, fixture(:user, 1))
      assert {:error, %Ecto.Changeset{}} = Contracts.update_contract(contract, @invalid_attrs)
    end

    test "delete_contract/1 deletes the contract" do
      contract = fixture(:contract, fixture(:user, 1))
      assert {:ok, %Contract{}} = Contracts.delete_contract(contract)
      assert_raise Ecto.NoResultsError, fn -> Contracts.get_contract!(contract.id) end
    end

    @tag :keke
    test "change_contract/1 returns a contract changeset" do
      contract = fixture(:contract, fixture(:user, 1))
      assert %Ecto.Changeset{} = Contracts.change_contract(contract)
    end
  end
end
