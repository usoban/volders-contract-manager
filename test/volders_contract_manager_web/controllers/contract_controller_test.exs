defmodule VoldersContractManagerWeb.ContractControllerTest do
  use VoldersContractManagerWeb.ConnCase

  alias VoldersContractManager.Accounts
  alias VoldersContractManager.Vendors
  alias VoldersContractManager.Categories
  alias VoldersContractManager.Contracts

  @vendor %{name: "Vodafone"}
  @category %{name: "Mobile"}
  
  @user1 %{email: "test@mailinator.com", name: "Test name", password: "testpassword", password_confirmation: "testpassword"}
  @user2 %{email: "test2@mailinator.com", name: "Test name 2", password: "testpassword", password_confirmation: "testpassword"}

  @create_attrs %{costs: "120.5", ends_at: Date.add(Date.utc_today, 2)}
  @update_attrs %{costs: "456.7", ends_at: Date.add(Date.utc_today, 2)}
  @invalid_attrs %{costs: nil, ends_at: nil}

  def fixture(:contract, %Accounts.User{id: uid}) do
    %{id: vid} = fixture(:vendor)
    %{id: cid} = fixture(:category)

    params = 
      @create_attrs
      |> Map.put(:vendor_id, vid)
      |> Map.put(:category_id, cid)
      |> Map.put(:user_id, uid)

    {:ok, contract} = Contracts.create_contract(params)
    contract
  end

  def fixture(:vendor) do
    {:ok, vendor} = Vendors.create_vendor(@vendor)
    vendor
  end

  def fixture(:category) do
    {:ok, category} = Categories.create_category(@category)
    category
  end

  def fixture(:user, 1) do
    {:ok, user} = Accounts.create_user(@user1)
    user
  end

  def fixture(:user, 2) do
    {:ok, user} = Accounts.create_user(@user2)
    user
  end

  def auth_user(conn, user) do
    {:ok, token, _} = VoldersContractManagerWeb.Guardian.encode_and_sign(user, %{}, token_type: :access)

    conn
    |> put_req_header("authorization", "bearer: " <> token)
  end

  describe "index" do
    test "lists all contracts", %{conn: conn} do
      conn = auth_user(conn, fixture(:user, 1))
      conn = get conn, contract_path(conn, :index)

      assert html_response(conn, 200) =~ "My contracts"
    end
  end

  describe "new contract" do
    test "renders form", %{conn: conn} do
      conn = auth_user(conn, fixture(:user, 1))
      conn = get conn, contract_path(conn, :new)
      assert html_response(conn, 200) =~ "New contract"
    end
  end

  describe "create contract" do
    test "redirects to index when data is valid", %{conn: conn} do
      %{id: vid} = fixture(:vendor)
      %{id: cid} = fixture(:category)

      conn = auth_user(conn, fixture(:user, 1))

      post_data = 
        @create_attrs
        |> Map.put(:vendor_id, vid)
        |> Map.put(:category_id, cid)

      conn = post conn, contract_path(conn, :create), contract: post_data

      assert redirected_to(conn) == contract_path(conn, :index)
    end

    test "renders errors when no vendor is given", %{conn: conn} do
      %{id: cid} = fixture(:category)

      conn = auth_user(conn, fixture(:user, 1))

      post_data = 
        @create_attrs
        |> Map.put(:category_id, cid)

      conn = post conn, contract_path(conn, :create), contract: post_data
      assert html_response(conn, 200) =~ "Oops, something went wrong! Please check the errors below."
    end

    test "renders errors when invalid vendor is given", %{conn: conn} do
      %{id: cid} = fixture(:category)

      conn = auth_user(conn, fixture(:user, 1))

      post_data = 
        @create_attrs
        |> Map.put(:category_id, cid)
        |> Map.put(:vendor_id, 12313213123)

      assert_raise Ecto.NoResultsError, fn -> 
          conn = post conn, contract_path(conn, :create), contract: post_data
       end
    end

    test "renders errors when no category is given", %{conn: conn} do
      %{id: vid} = fixture(:vendor)

      conn = auth_user(conn, fixture(:user, 1))

      post_data = 
        @create_attrs
        |> Map.put(:vendor_id, vid)

      conn = post conn, contract_path(conn, :create), contract: post_data
      assert html_response(conn, 200) =~ "Oops, something went wrong! Please check the errors below."
    end

    test "renders errors when cost is below 0", %{conn: conn} do
      %{id: vid} = fixture(:vendor)
      %{id: cid} = fixture(:category)

      conn = auth_user(conn, fixture(:user, 1))

      post_data = 
        @create_attrs
        |> Map.put(:vendor_id, vid)
        |> Map.put(:category_id, cid)
        |> Map.put(:costs, -0.1)

      conn = post conn, contract_path(conn, :create), contract: post_data
      assert html_response(conn, 200) =~ "Costs should be a positive number, greater than 0."
    end

    test "renders errors when date is not in the future", %{conn: conn} do
      %{id: vid} = fixture(:vendor)
      %{id: cid} = fixture(:category)

      conn = auth_user(conn, fixture(:user, 1))

      post_data = 
        @create_attrs
        |> Map.put(:vendor_id, vid)
        |> Map.put(:category_id, cid)
        |> Map.put(:ends_at, Date.utc_today)

      conn = post conn, contract_path(conn, :create), contract: post_data
      assert html_response(conn, 200) =~ "Date should be in the future."
    end
  end

  describe "edit contract" do
    setup [:create_contract]

    test "renders form for editing chosen contract", %{conn: conn, contract: contract, user1: user} do
      conn = auth_user(conn, user)
      
      conn = get conn, contract_path(conn, :edit, contract)
      assert html_response(conn, 200) =~ "Edit Contract"
    end
  end

  describe "update contract" do
    setup [:create_contract]

    test "redirects when data is valid", %{conn: conn, contract: contract, user1: user} do
      conn = auth_user(conn, user)
      conn = put conn, contract_path(conn, :update, contract), contract: @update_attrs
      assert redirected_to(conn) == contract_path(conn, :index)
    end

    test "renders errors when data is invalid", %{conn: conn, contract: contract, user1: user} do
      conn = auth_user(conn, user)
      conn = put conn, contract_path(conn, :update, contract), contract: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit Contract"
    end
  end

  describe "delete contract" do
    setup [:create_contract]

    test "deletes chosen contract", %{conn: conn, contract: contract, user1: u1, user2: u2} do
      conn = auth_user(conn, u1)
      conn = delete conn, contract_path(conn, :delete, contract)
      assert redirected_to(conn) == contract_path(conn, :index)
    end
  end

  defp create_contract(_) do
    user1 = fixture(:user, 1)
    user2 = fixture(:user, 2)
    contract = fixture(:contract, user1)
  
    {:ok, contract: contract, user1: user1, user2: user2}
  end
end
