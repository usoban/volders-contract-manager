defmodule VoldersContractManagerWeb.UserControllerTest do
  use VoldersContractManagerWeb.ConnCase

  alias VoldersContractManager.Accounts

  @create_attrs %{email: "test@mailinator.com", name: "Test name", password: "testpassword", password_confirmation: "testpassword"}
  @invalid_attrs %{email: nil, name: nil, password_hash: nil, password_salt: nil}

  def fixture(:user) do
    {:ok, user} = Accounts.create_user(@create_attrs)
    user
  end
  
  describe "new user" do
    test "renders form", %{conn: conn} do
      conn = get conn, user_path(conn, :new)
      assert html_response(conn, 200) =~ "Sign up"
    end
  end

  describe "create user" do
    test "redirects to contract listing when data is valid", %{conn: conn} do
      conn = post conn, user_path(conn, :create), user: @create_attrs
      
      assert redirected_to(conn) == contract_path(conn, :index)
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, user_path(conn, :create), user: @invalid_attrs
      assert html_response(conn, 200) =~ "Sign up"
    end
  end

  describe "login user" do
    setup [:create_user]

    test "renders form for singing in", %{conn: conn, user: user} do
      conn = get conn, user_path(conn, :signin)
    
      assert html_response(conn, 200) =~ "Email"
    end
  end

  defp create_user(_) do
    user = fixture(:user)
    {:ok, user: user}
  end
end
