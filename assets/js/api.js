class VendorsApi {

  constructor(apiKey) {
    this.apiKey = apiKey;
  } 

  categories(vendorId, cb) {
    let req = new XMLHttpRequest();

    req.addEventListener('load', cb);
    req.open('GET', `/api/vendors/${vendorId}/categories`);
    req.setRequestHeader('Authorization', 'Bearer ' + this.apiKey);
    req.send();
  }
}

module.exports = {
  VendorsApi: VendorsApi
};