class SelectLoader {

  constructor(primarySelectId, dependentSelectId, loadFn) {
    this.primarySelectId = primarySelectId;
    this.dependentSelectId = dependentSelectId; 
    this.loadFn = loadFn;
  }

  init() {
    document.addEventListener('DOMContentLoaded', () => {
      this.primarySelect = document.getElementById(this.primarySelectId);
      this.dependentSelect = document.getElementById(this.dependentSelectId);

      this.listen();
    });
  }

  listen() {
    this.primarySelect.onchange = () => {
      this.populateDependentSelect();
    };
  }

  populateDependentSelect() {
    this.loadFn(this.primarySelect.value, (results) => {
      let newOptions = this.makeOptions(results);

      while (this.dependentSelect.firstChild) {
        this.dependentSelect.removeChild(this.dependentSelect.firstChild);
      }

      newOptions.forEach((o) => {
        this.dependentSelect.appendChild(o);
      });
    });
  }

  makeOptions(collection) {
    return collection.map((c) => {
      let option = document.createElement('option');
      
      option.value = c.id;
      option.innerText = c.name;

      return option;
    });
  }
}

module.exports = {
  SelectLoader: SelectLoader
};