# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     VoldersContractManager.Repo.insert!(%VoldersContractManager.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias VoldersContractManager.Repo
alias VoldersContractManager.Vendors.Vendor
alias VoldersContractManager.Categories.Category

internet = Repo.insert! %Category{name: "Internet"}
dsl = Repo.insert! %Category{name: "DSL"}
phone = Repo.insert! %Category{name: "Phone"}
mobile_phone = Repo.insert! %Category{name: "Mobile phone"}
electricity = Repo.insert! %Category{name: "Electricity"}
gas = Repo.insert! %Category{name: "Gas"}
gym = Repo.insert! %Category{name: "Gym"}
paid_tv = Repo.insert! %Category{name: "Paid TV"}

_vodafone = Repo.insert! %Vendor{name: "Vodafone", categories: [internet, dsl, phone, mobile_phone]}
_o2 = Repo.insert! %Vendor{name: "O2", categories: [internet, dsl]}
_vattenfall = Repo.insert! %Vendor{name: "Vattenfall", categories: [electricity, gas]}
_mcfit = Repo.insert! %Vendor{name: "McFit", categories: [gym]}
_sky =Repo.insert! %Vendor{name: "Sky", categories: [paid_tv]}
