defmodule VoldersContractManager.Repo.Migrations.CreateVendorsCategories do
  use Ecto.Migration

  def change do
    create table(:vendors_categories) do
      add :vendor_id, references(:vendors, on_delete: :nothing), null: false
      add :category_id, references(:categories, on_delete: :nothing), null: false

      timestamps()
    end

    create index(:vendors_categories, [:vendor_id])
    create index(:vendors_categories, [:category_id])
  end
end
