defmodule VoldersContractManager.Repo.Migrations.CreateVendors do
  use Ecto.Migration

  def change do
    create table(:vendors) do
      add :name, :string, null: false

      timestamps()
    end

    create unique_index(:vendors, [:name])
  end
end
