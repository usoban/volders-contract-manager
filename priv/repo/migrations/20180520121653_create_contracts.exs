defmodule VoldersContractManager.Repo.Migrations.CreateContracts do
  use Ecto.Migration

  def change do
    create table(:contracts) do
      add :costs, :decimal, null: false
      add :ends_at, :date, null: false
      add :user_id, references(:users, on_delete: :nothing), null: false
      add :vendor_id, references(:vendors, on_delete: :nothing), null: false
      add :category_id, references(:categories, on_delete: :nothing), null: false

      timestamps()
    end

    create index(:contracts, [:user_id])
    create index(:contracts, [:vendor_id])
    create index(:contracts, [:category_id])
  end
end
