# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :volders_contract_manager,
  ecto_repos: [VoldersContractManager.Repo]

# Configures the endpoint
config :volders_contract_manager, VoldersContractManagerWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "d483r5SBam6dgJJp3pblgAt06ZnleE52g/ASOgcXvozfUqU5Z2iVSwHPNjw/wkOQ",
  render_errors: [view: VoldersContractManagerWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: VoldersContractManager.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]


config :volders_contract_manager, VoldersContractManagerWeb.Guardian,
  issuer: "volders_contract_manager",
  secret_key: "WVNDweGz+/ti1GwmJxwhlrzMIoNDhGJPxrWKhfva7Q82IF1aNzrLZoexbqlela3N"

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
