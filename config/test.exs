use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :volders_contract_manager, VoldersContractManagerWeb.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :volders_contract_manager, VoldersContractManager.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "ngrts",
  password: "ngrts",
  database: "volders_contract_manager_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
