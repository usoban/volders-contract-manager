defmodule VoldersContractManagerWeb.Guardian do
  use Guardian, otp_app: :volders_contract_manager

  alias VoldersContractManager.Accounts.User

  def subject_for_token(%User{} = user, _claims) do
    {:ok, user.email}
  end

  def resource_from_claims(claims) do
    email = claims["sub"]

    case VoldersContractManager.Accounts.get_user_by(email: email) do
      nil ->
        {:error, :resource_missing}

      user ->
        {:ok, user}
    end
  end

  def current_user!(conn) do
    Guardian.Plug.current_resource(conn)
  end

  def user_signed_in?(conn) do
    case Guardian.Plug.current_resource(conn) do
      %User{} -> true
      _ -> false
    end
  end
end