defmodule VoldersContractManagerWeb.FormHelpers do
  def state_class(form, field) do
    cond do
      !form.source.action -> ""
      form.errors[field] -> "has-error"
      true -> ""
    end
  end

  def to_select_options(lst) when is_list(lst) do
    to_select_options(lst, false)
  end

  def to_select_options(lst, false) when is_list(lst) do
    lst |> Enum.map(fn e -> {e.name, e.id} end) 
  end

  def to_select_options(lst, placeholder_label) when is_list(lst) do
    placeholder = [
      key: placeholder_label,
      value: '',
      disabled: true,
      selected: true
    ]

    [placeholder | to_select_options(lst, false)]
  end

  def format_currency(amount) do
    {:ok, fmt} = Cldr.Number.to_string amount, locale: "en", currency: "USD"
    fmt
  end

  def format_date(date) do
    {:ok, fmt} = Cldr.Date.to_string date, locale: "en"
    fmt
  end
end