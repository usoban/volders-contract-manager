defmodule VoldersContractManagerWeb.CategoryView do
  use VoldersContractManagerWeb, :view

  def render("list.json", %{categories: categories}) do
    %{data: render_many(categories, __MODULE__, "category.json")}
  end

  def render("category.json", %{category: category}) do
    %{
      id: category.id,
      name: category.name
    }
  end
end
