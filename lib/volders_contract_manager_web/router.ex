defmodule VoldersContractManagerWeb.Router do
  use VoldersContractManagerWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug Plug.Session, store: :cookie,
                  key: "_vcontractmanager",
                  encryption_salt: "supersecretsalt",
                  signing_salt: "supersecretsigningsalt",
                  key_length: 64,
                  log: :debug
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :auth do
    plug Guardian.Plug.Pipeline, [module: VoldersContractManagerWeb.Guardian,
                                 error_handler: VoldersContractManagerWeb.AuthErrorHandler]
                                 
    plug Guardian.Plug.VerifyHeader, claims: %{"typ" => "access"}
    plug Guardian.Plug.VerifySession, claims: %{"typ" => "access"}
    plug Guardian.Plug.EnsureAuthenticated, claims: %{"typ" => "access"}
    plug Guardian.Plug.LoadResource, claims: %{"typ" => "access"}
  end

  scope "/", VoldersContractManagerWeb do
    pipe_through :browser

    get "/", Redirect, to: "/manager/contracts"

    get "/sign-up", UserController, :new
    get "/sign-in", UserController, :signin
    get "/sign-out", UserController, :signout
    post "/login", UserController, :login
    
    resources "/users", UserController, only: [:create]
  end

  scope "/manager", VoldersContractManagerWeb do
    pipe_through :browser
    pipe_through :auth

    resources "/vendors", VendorController
    resources "/categories", CategoryController
    resources "/contracts", ContractController
  end

  scope "/api", VoldersContractManagerWeb do
    pipe_through :api
    pipe_through :auth

    resources "/vendors", VendorController, only: [] do
      resources "/categories", Vendor.VendorCategoryController, only: [:index]
    end
  end
end
