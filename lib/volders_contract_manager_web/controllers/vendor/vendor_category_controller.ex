defmodule VoldersContractManagerWeb.Vendor.VendorCategoryController do
  use VoldersContractManagerWeb, :controller

  alias VoldersContractManager.Vendors

  def index(conn, %{"vendor_id" => vendor_id}) do
    categories = Vendors.list_vendor_categories(vendor_id)

    conn
    |> put_view(VoldersContractManagerWeb.CategoryView)
    |> render("list.json", categories: categories)
  end
end