defmodule VoldersContractManagerWeb.ContractController do
  use VoldersContractManagerWeb, :controller

  alias VoldersContractManagerWeb.{Endpoint, ContractFallbackController}
  alias VoldersContractManager.Contracts
  alias VoldersContractManager.Contracts.{Contract, Auth}
  alias VoldersContractManager.Vendors
  alias VoldersContractManager.Vendors.Vendor
  alias VoldersContractManagerWeb.Guardian

  action_fallback ContractFallbackController

  def index(conn, _params) do
    current_user = Guardian.current_user!(conn)
    contracts = Contracts.list_active_user_contracts(current_user)

    render(conn, "index.html", contracts: contracts)
  end

  def new(conn, _params) do
    conn
    |> render("new.html", %{changeset: Contracts.make_contract()} |> prepare_data)
  end

  def create(conn, %{"contract" => contract_params}) do
    current_user = Guardian.current_user!(conn)
    new_contract = Contracts.create_user_contract(current_user, contract_params)

    case new_contract do
      {:ok, _} ->
        conn
        |> put_flash(:info, "Contract created successfully.")
        |> redirect(to: contract_path(conn, :index))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", %{changeset: changeset} |> prepare_data)
    end
  end

  def show(conn, %{"id" => id}) do
    with current_user <- Guardian.current_user!(conn),
        {:ok, contract} <- Contracts.get_contract(id),
        true <- Auth.authorized?(contract, current_user) do
      conn |> render("show.html", contract: contract)
    end
  end

  def edit(conn, %{"id" => id}) do
    with current_user <- Guardian.current_user!(conn),
        {:ok, contract} <- Contracts.get_contract(id),
        true <- Auth.authorized?(contract, current_user) do
      changeset = Contracts.change_contract(contract)
      render(conn, "edit.html", %{contract: contract, changeset: changeset} |> prepare_data)
    end
  end

  def update(conn, %{"id" => id, "contract" => contract_params}) do
    with current_user <- Guardian.current_user!(conn),
         {:ok, contract} <- Contracts.get_contract(id),
         true <- Auth.authorized?(contract, current_user) do
      case Contracts.update_contract(contract, contract_params) do
        {:ok, _} ->
          conn
          |> put_flash(:info, "Contract updated successfully.")
          |> redirect(to: contract_path(conn, :index))
        {:error, %Ecto.Changeset{} = changeset} ->
          render(conn, "edit.html", %{contract: contract, changeset: changeset} |> prepare_data)
      end
    end
  end

  def delete(conn, %{"id" => id}) do
    with current_user <- Guardian.current_user!(conn),
         {:ok, contract} <- Contracts.get_contract(id),
         true <- Auth.authorized?(contract, current_user) do

      {:ok, _contract} = Contracts.delete_contract(contract)

      conn
      |> put_flash(:info, "Contract deleted successfully.")
      |> redirect(to: contract_path(conn, :index))
    end
  end

  defp prepare_data(data) when is_map(data) do
    data = 
      data
      |> Map.put(:vendors, Vendors.list_vendors())
      |> Map.put(:categories, [])

    prepare_categories(data)
  end

  defp prepare_categories(%{changeset: %Ecto.Changeset{changes: %{vendor_id: vendor_id}}} = data) do
    %{data | categories: Vendors.list_vendor_categories(vendor_id)}
  end

  defp prepare_categories(%{contract: %Contract{vendor: %Vendor{id: vendor_id}}} = data) do
    %{data | categories: Vendors.list_vendor_categories(vendor_id)}
  end

  defp prepare_categories(data), do: data
end
