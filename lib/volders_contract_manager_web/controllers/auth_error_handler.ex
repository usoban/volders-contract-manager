defmodule VoldersContractManagerWeb.AuthErrorHandler do
  use VoldersContractManagerWeb, :controller

  def auth_error(conn, {type, reason}, _opts) do
    case get_format(conn) do
      "json" ->
        conn
        |> send_resp(401, "")

      _ ->
        conn
        |> put_flash(:error, "Please sign in before continuing.")
        |> redirect(to: user_path(conn, :signin))
    end
  end
end