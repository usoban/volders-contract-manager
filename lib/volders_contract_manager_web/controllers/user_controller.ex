defmodule VoldersContractManagerWeb.UserController do
  use VoldersContractManagerWeb, :controller

  alias VoldersContractManager.Accounts
  alias VoldersContractManager.Accounts.User
  alias VoldersContractManagerWeb.Guardian

  def signin(conn, _params) do
    render(conn, "sign_in.html")
  end

  def login(conn, %{"email" => email, "password" => password}) do
    case Accounts.user_login_valid?(email, password) do
      true ->
        conn
        |> put_flash(:info, "Login successful.")
        |> login_and_redirect(%User{email: email})
      false ->
        conn
        |> put_flash(:error, "Combination of email and password does not match.")
        |> render("sign_in.html")
    end
  end

  def new(conn, _params) do
    changeset = Accounts.change_user(%User{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"user" => user_params}) do
    case Accounts.create_user(user_params) do
      {:ok, %User{} = user} ->
        conn
        |> put_flash(:info, "User created successfully.")
        |> login_and_redirect(user)
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def signout(conn, _params) do
    conn
    |> Guardian.Plug.sign_out
    |> redirect(to: contract_path(conn, :index))
  end

  defp login_and_redirect(conn, %User{} = user) do
    conn
    |> Guardian.Plug.sign_in(user)
    |> redirect(to: contract_path(conn, :index))
  end
end
