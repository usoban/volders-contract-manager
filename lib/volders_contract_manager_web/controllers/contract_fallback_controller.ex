defmodule VoldersContractManagerWeb.ContractFallbackController do
  use Phoenix.Controller

  def call(conn, {:error, :unauthorized}) do
    conn
    |> put_status(403)
    |> render(VoldersContractManagerWeb.ErrorView, :"403")
  end

  def call(conn, {:error, :not_found}) do
    conn
    |> put_status(404)
    |> render(VoldersContractManagerWeb.ErrorView, :"404")
  end
end