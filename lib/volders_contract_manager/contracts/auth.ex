defmodule VoldersContractManager.Contracts.Auth do
  alias VoldersContractManager.Contracts.Contract
  alias VoldersContractManager.Accounts.User

  def authorized?(%Contract{user_id: uid}, %User{id: uid}), do: true
  def authorized?(_, _), do: {:error, :unauthorized}

end