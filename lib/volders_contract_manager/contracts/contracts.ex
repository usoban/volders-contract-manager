defmodule VoldersContractManager.Contracts do
  @moduledoc """
  The Contracts context.
  """

  import Ecto.Query, warn: false
  alias VoldersContractManager.Repo

  alias VoldersContractManager.Contracts.Contract
  alias VoldersContractManager.Accounts.User

  @doc """
  Returns the list of contracts.

  ## Examples

      iex> list_contracts()
      [%Contract{}, ...]

  """
  def list_contracts(queryable \\ Contract) do
    queryable 
    |> preload(:vendor)
    |> preload(:category)
    |> Repo.all
  end

  defp user_contracts(%User{id: uid}) do
    from c in Contract, where: c.user_id == ^uid
  end

  def list_user_contracts(%User{} = user) do
    user
    |> user_contracts
    |> list_contracts
  end

  def active_user_contracts(%User{} = user) do
    current_date = Date.utc_today

    from c in user_contracts(user), where: c.ends_at >= ^current_date
  end

  def list_active_user_contracts(%User{} = user, order_by \\ [asc: :ends_at]) do
    user 
    |> active_user_contracts
    |> order_by(^order_by)
    |> list_contracts
  end

  @doc """
  Gets a single contract.

  Raises `Ecto.NoResultsError` if the Contract does not exist.

  ## Examples

      iex> get_contract!(123)
      %Contract{}

      iex> get_contract!(456)
      ** (Ecto.NoResultsError)

  """
  def get_contract!(id) do
    Repo.get!(Contract, id) |> Repo.preload([:vendor, :category])
  end

  def get_contract(id) do
    result = Repo.get(Contract, id) |> Repo.preload([:vendor, :category])

    case result do
      %Contract{} = c -> {:ok, c}
      nil -> {:error, :not_found}
    end
  end

  @doc """
  Creates a contract.

  ## Examples

      iex> create_contract(%{field: value})
      {:ok, %Contract{}}

      iex> create_contract(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_contract(attrs \\ %{}) do
    make_contract(attrs)
    |> Repo.insert()
  end

  def create_user_contract(%User{id: uid}, attrs \\ %{}) do
    attrs
    |> Map.put("user_id", uid)
    |> create_contract
  end

  @doc """
  Updates a contract.

  ## Examples

      iex> update_contract(contract, %{field: new_value})
      {:ok, %Contract{}}

      iex> update_contract(contract, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_contract(%Contract{} = contract, attrs) do
    contract
    |> Contract.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Contract.

  ## Examples

      iex> delete_contract(contract)
      {:ok, %Contract{}}

      iex> delete_contract(contract)
      {:error, %Ecto.Changeset{}}

  """
  def delete_contract(%Contract{} = contract) do
    Repo.delete(contract)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking contract changes.

  ## Examples

      iex> change_contract(contract)
      %Ecto.Changeset{source: %Contract{}}

  """
  def change_contract(%Contract{} = contract) do
    Contract.changeset(contract, %{})
  end

  def make_contract(attrs \\ %{}) do
    %Contract{}
    |> Contract.changeset(attrs)
  end
end
