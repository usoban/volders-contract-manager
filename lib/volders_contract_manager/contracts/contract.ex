defmodule VoldersContractManager.Contracts.Contract do
  use Ecto.Schema
  import Ecto.Changeset

  schema "contracts" do
    belongs_to :user, VoldersContractManager.Accounts.User
    belongs_to :vendor, VoldersContractManager.Vendors.Vendor   
    belongs_to :category, VoldersContractManager.Categories.Category

    field :costs, :decimal
    field :ends_at, :date

    timestamps()
  end

  def changeset(contract, attrs) do
    contract
    |> cast(attrs, [:costs, :ends_at, :user_id, :vendor_id, :category_id])
    |> validate_required([:costs, :ends_at, :user_id, :vendor_id, :category_id])
    |> validate_number(:costs, greater_than: 0.00, message: "Costs should be a positive number, greater than 0.")
    |> validate_future_date(:ends_at)
    |> assoc_constraint(:user)
    |> assoc_constraint(:vendor, message: "Selected vendor does not exist.")
    |> assoc_constraint(:category, message: "Selected category does not exist.")
  end

  defp validate_future_date(%Ecto.Changeset{changes: changes} = changeset, field) do
    if Map.has_key?(changes, field) do
      do_validate_future_date(changeset, field, Map.get(changes, field))
    else
      changeset
    end
  end

  defp do_validate_future_date(%Ecto.Changeset{} = changeset, field, field_value) do
    case Date.compare(Date.utc_today, field_value) do
      :lt ->
        changeset
      _ ->
        changeset |> add_error(field, "Date should be in the future.")
    end
  end
end
