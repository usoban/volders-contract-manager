defmodule VoldersContractManager.Vendors.Vendor do
  use Ecto.Schema
  import Ecto.Changeset

  alias VoldersContractManager.Categories.Category
  alias VoldersContractManager.Vendors.VendorCategory

  schema "vendors" do
    field :name, :string
    many_to_many :categories, Category, join_through: VendorCategory

    timestamps()
  end

  @doc false
  def changeset(vendor, attrs) do
    vendor
    |> cast(attrs, [:name])
    |> validate_required([:name])
    |> unique_constraint(:name)
  end
end
