defmodule VoldersContractManager.Vendors.VendorCategory do
  use Ecto.Schema
  import Ecto.Changeset
  
  alias VoldersContractManager.Vendors.Vendor
  alias VoldersContractManager.Categories.Category

  schema "vendors_categories" do
    belongs_to :vendor, Vendor
    belongs_to :category, Category

    timestamps()
  end

  @doc false
  def changeset(vendor_category, attrs) do
    vendor_category
    |> cast(attrs, [])
    |> validate_required([])
  end
end
