defmodule VoldersContractManager.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset

  # https://www.w3.org/TR/html5/forms.html#valid-e-mail-address
  @email_validation_regex ~r/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/

  schema "users" do
    field :email, :string
    field :name, :string
    field :password_hash, :string
    field :password, :string, virtual: true
    field :password_confirmation, :string, virtual: true

    timestamps()
  end

  def registration_changeset(user, attrs) do
    user
    |> cast(attrs, [:email, :name, :password, :password_confirmation])
    |> validate_required([:email, :name, :password, :password_confirmation])
    |> validate_length(:email, max: 255)
    |> validate_length(:password, min: 8)
    |> validate_length(:name, [min: 2, max: 255])
    |> validate_format(:email, @email_validation_regex, message: "Email is not valid.")
    |> unique_constraint(:email, message: "Email has already been taken.")
    |> validate_confirmation(:password, [required: true, message: "Password and confirmation do not match."])
    |> hash_password
  end

  defp hash_password(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: password}} ->
        changeset
        |> put_change(:password_hash, Bcrypt.hash_pwd_salt(password))

      _ ->
        changeset
    end
  end
end
